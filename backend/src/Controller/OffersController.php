<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Offers;
use DateTime;

class OffersController extends AbstractController
{
    #[Route('api/offers/{id}', methods: ['GET', 'HEAD'], name: 'single_offer')]
    public function index(ManagerRegistry $doctrine,  $id): Response
    {
        $offer = $doctrine ->getrepository(Offers::class)->find($id);
        //dd($offer);
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/OffersController.php',
        ]);
    }
    // -- /api/offers (GET)
    #[Route('/api/offers',methods: ['GET', 'HEAD'], name: 'all_offers')]
    public function offers(ManagerRegistry $doctrine): Response
    {
        $offers = $doctrine->getRepository(Offers::class)->findAll();

        foreach($offers as $item) {
            $arrayCollection[] = array(
                'id' => $item->getId(),
                'title'=> $item->getTitle(),
                'type'=>$item->getType(),
                'fonction'=>$item->getFonction(),
                'description'=>$item->getDescription(),
                'ville'=>$item->getVille(),
                'date'=>$item->getDate(),
            );
       }

        return $this->json([
            'message' => 'test',
            'data' => $arrayCollection,
        ]);
    }
    //--route meant to add offer to db manualy
    #[Route('/add-offers', methods: ['POST', 'HEAD'], name: 'add_offer')]
    public function addOffer(ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $offer = new Offers();
        $offer->setTitle('Developper web');
        $offer->setType('CDI');
        $offer->setFonction('FullStack');
        $offer->setDescription('tu participeras à toutes les étapes de la vie du projet, de la conception à l intégration jusqu aux échanges avec le client.
        Ton environnement technique va du backend au frontend avec différents langages de développement : java, php, net, Angular, Node...');
        $offer->setVille('Toulouse');
        $objDateTime = new DateTime('NOW');
        $offer->setDate($objDateTime);

        $entityManager->persist($offer);
        $entityManager->flush();

        //dd($offer);
        return $this->json([
            'message' => $offer->getId()."",
            'path' => 'src/Controller/OffersController.php',
        ]);
    }
   
    // -- /api/application/[offer_id] (POST)


    // -- /api/my-applications (GET)
    #[Route('/api/my-applications', name: 'app-applications')]
    public function myApplications(): Response
    {
        return $this->json([
            'message' => 'Welcome to your applications!',
            'path' => 'src/Controller/OffersController.php',
        ]);
    }
    // -- /api/fav/my-offers (GET)
    #[Route('/api/fav/my-offers',methods: ['GET', 'HEAD'], name: 'myFavoriteOffers')]
    public function MyFavoriteOffers(): Response
    {
        return $this->json([
            'message' => 'Welcome to MyFavoriteOffers route!',
            'path' => 'src/Controller/OffersController.php',
        ]);
    }
    // -- /api/fav/[offer_id] (POST/DELETE)
    #[Route('/api/fav/{offer_id}',methods: ['POST', 'HEAD'], name: 'addOfferToFavorites')]
    public function AddOfferToFavorite(): Response
    {
        return $this->json([
            'message' => 'Welcome to add FavoriteOffers route!',
            'path' => 'src/Controller/OffersController.php',
        ]);
    }
    // -- /api/fav/[offer_id] (DELETE)
    #[Route('/api/fav/delete/{offer_id}',methods: ['DELETE', 'HEAD'], name: 'removeOfferFromFavorites')]
    public function RemoveOfferFromFavorite(): Response
    {
        return $this->json([
            'message' => 'Welcome to delete FavoriteOffers route!',
            'path' => 'src/Controller/OffersController.php',
        ]);
    }
}

