<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use \Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Users;
use DateTime;

class UsersController extends AbstractController
{

    //--route meant to add user to db manualy
    #[Route('/add-user',methods: ['POST', 'HEAD'],  name: 'add-user')]
    public function addUser(ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $user = new Users();
        $user->setEmail('William.croixpas@stars.com');
        $user->setPassword('12345');
        $user->setFirstName('William');
        $user->setLastName('Anderson');
        $user->setPhone('333666999');
        $user->setAdress('Route 4 , California ');
        $user->setCodPostal('31254');
        $user->setCity('Hollywood');
        $user->setLinkedin('linkedin.com/PamelaAnderson');
        $user->setGitLabHub('gitlab.com/PA');

        $entityManager->persist($user);
        $entityManager->flush();
        // dd($user);
        return $this->json([
            'message' => $user->getId(). "Welcome to your new controller!",
            'path' => 'src/Controller/UsersController.php',
        ]);
    }


    // -- /api/login (GET)
    #[Route('api/login', methods: ['POST', 'HEAD'], name: 'login')]
    public function Login($username = 'user', $password = 'password'): Response
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
              '_username' => $username,
              '_password' => $password,
            ])
          );
      
          $data = json_decode($client->getResponse()->getContent(), true);
          dd($data);
          $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));
      
          return $client;
    }


    // -- /api/register (POST)
    #[Route('api/register', methods: ['POST', 'HEAD'], name: 'register')]
    public function Register(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $value = json_decode($request->getContent(), true);

        $username = $value['email'];
        $password = $value['password'];

        $repository = $doctrine->getRepository(Users::class);
         $user = $repository->findOneBy(['username' => $username]);

         if($user != null){
             return new Response('User already exists');
         }

        $newUser = new Users();

        $newUser->setUsername($username);
        $newUser->setPassword($password);

        $entityManager->persist($newUser);
        $entityManager->flush();

        return new Response('User saved successfully');
    }

    public function getTokenUser(Security $security, JWTTokenManagerInterface $JWTManager)
    {
        $user = new Users('a@a', 'test');
        //dd($user);
        return $this->json(['token' => $JWTManager->create($user)]);
    }




}
