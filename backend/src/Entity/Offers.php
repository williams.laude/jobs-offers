<?php

namespace App\Entity;

use App\Repository\OffersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OffersRepository::class)]
class Offers
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'string', length: 255)]
    private $type;

    #[ORM\Column(type: 'string', length: 255)]
    private $fonction;

    #[ORM\Column(type: 'string', length: 2000)]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    private $ville;

    #[ORM\Column(type: 'date')]
    private $date;

    #[ORM\ManyToMany(targetEntity: Users::class, mappedBy: 'candidature')]
    private $candidatureId;

    #[ORM\ManyToMany(targetEntity: Users::class, mappedBy: 'favorite')]
    private $favoriteId;

    public function __construct()
    {
        $this->candidatureId = new ArrayCollection();
        $this->favoriteId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFonction(): ?string
    {
        return $this->fonction;
    }

    public function setFonction(string $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, Users>
     */
    public function getCandidatureId(): Collection
    {
        return $this->candidatureId;
    }

    public function addCandidatureId(Users $candidatureId): self
    {
        if (!$this->candidatureId->contains($candidatureId)) {
            $this->candidatureId[] = $candidatureId;
        }

        return $this;
    }

    public function removeCandidatureId(Users $candidatureId): self
    {
        $this->candidatureId->removeElement($candidatureId);

        return $this;
    }

    /**
     * @return Collection<int, Users>
     */
    public function getFavoriteId(): Collection
    {
        return $this->favoriteId;
    }

    public function addFavoriteId(Users $favoriteId): self
    {
        if (!$this->favoriteId->contains($favoriteId)) {
            $this->favoriteId[] = $favoriteId;
        }

        return $this;
    }

    public function removeFavoriteId(Users $favoriteId): self
    {
        $this->favoriteId->removeElement($favoriteId);

        return $this;
    }
}
