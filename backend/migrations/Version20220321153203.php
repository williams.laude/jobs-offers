<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220321153203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE offers (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, fonction VARCHAR(255) NOT NULL, description VARCHAR(2000) NOT NULL, ville VARCHAR(255) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone INT DEFAULT NULL, adress VARCHAR(255) NOT NULL, cod_postal INT NOT NULL, city VARCHAR(255) NOT NULL, linkedin VARCHAR(255) DEFAULT NULL, git_lab_hub VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_offers (users_id INT NOT NULL, offers_id INT NOT NULL, INDEX IDX_D5A18AB767B3B43D (users_id), INDEX IDX_D5A18AB7A090B42E (offers_id), PRIMARY KEY(users_id, offers_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_offers_favorite (users_id INT NOT NULL, offers_id INT NOT NULL, INDEX IDX_BD4AAB967B3B43D (users_id), INDEX IDX_BD4AAB9A090B42E (offers_id), PRIMARY KEY(users_id, offers_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_offers ADD CONSTRAINT FK_D5A18AB767B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_offers ADD CONSTRAINT FK_D5A18AB7A090B42E FOREIGN KEY (offers_id) REFERENCES offers (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_offers_favorite ADD CONSTRAINT FK_BD4AAB967B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_offers_favorite ADD CONSTRAINT FK_BD4AAB9A090B42E FOREIGN KEY (offers_id) REFERENCES offers (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users_offers DROP FOREIGN KEY FK_D5A18AB7A090B42E');
        $this->addSql('ALTER TABLE users_offers_favorite DROP FOREIGN KEY FK_BD4AAB9A090B42E');
        $this->addSql('ALTER TABLE users_offers DROP FOREIGN KEY FK_D5A18AB767B3B43D');
        $this->addSql('ALTER TABLE users_offers_favorite DROP FOREIGN KEY FK_BD4AAB967B3B43D');
        $this->addSql('DROP TABLE offers');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE users_offers');
        $this->addSql('DROP TABLE users_offers_favorite');
    }
}
