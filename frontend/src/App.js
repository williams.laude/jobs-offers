import React from 'react';
import './App.css';
import Navbar from './components/general/navbar';
import Acceuil from './components/pages/Acceuil';
import Offers from './components/pages/Offres';
import MyApplications from './components/pages/Apply';
import Favorites from './components/pages/Favorites';
import Contact from './components/pages/Contact';
import Footer from './components/general/footer';
import ContactUsSend from './components/pages/contactUsSend';
import ApplyForm from './components/pages/ApplyForm';
import SignUp from './components/pages/SignUp';
import SignIn from './components/pages/SignIn';
import DetailOffer from'./components/pages/DetailOffer'

 export default class App extends React.Component {
   constructor(props) {
     super(props)
     this.state = {
       page:'Acceuil',
       offers:[],
       detailOffer: undefined,
       offerId: undefined
     };
     this.handlePageChange = this.handlePageChange.bind(this);
     this.handleApplyOffer = this.handleApplyOffer.bind(this);
    
   }

  
   handlePageChange(string) {
    this.setState({page: string});
  }

  handleApplyOffer(offerId){
    this.setState({
      offerId: offerId,
      page: 'ApplyForm'
    });
  }
//   resetDetailOffer() {
//     console.log('resetdetailOffer: ', this);
//     this.setState({
//         detailOffer: undefined
//     });
// }

handleOfferId(id) {
  const offer = this.state.offers.filter(e => e.id === id)[0];
  if (offer) {
      this.setState({
          detailOffer: offer
      });
  }
  console.log('ID:', id)
}

  render() {
    return (
      <div className="App">
        <>
          <Navbar callback={this.handlePageChange} />
          {this.state.page == "Acceuil" && <Acceuil  handleApplyOffer={this.handleApplyOffer} callback={this.handlePageChange}/>}
          {this.state.page == "MyAppli" && <MyApplications callback={this.handlePageChange}/>}
          {this.state.page == "Offres" && <Offers handleApplyOffer={this.handleApplyOffer} />}
          {this.state.page == "Favorites" && <Favorites handleApplyOffer={this.handleApplyOffer} callback={this.handlePageChange} />}
          {this.state.page == "Contact" && <Contact callback={this.handlePageChange}/>}
          {this.state.page == "ContactUsSend" && <ContactUsSend />}
          {this.state.page == "SignUp" && <SignUp callback={this.handlePageChange}/>}
          {this.state.page == "SignIn" && <SignIn callback={this.handlePageChange}/>}
          {this.state.page == "ApplyForm" && this.state.offerId !== undefined && <ApplyForm offerId={this.state.offerId} />}
          
          <div>
            <Footer />
          </div>
        </>
      </div>
    )
  }
}

