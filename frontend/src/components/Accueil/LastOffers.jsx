
import React, { Component } from 'react';
import FavoriteStar from '../general/favoriteStar';
import DetailOffer from '../pages/DetailOffer';


class LastOffers extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            error: null,
            offers: [],
            detailOffer: undefined,
        };
        this.resetDetailOffer= this.resetDetailOffer.bind(this)
    }
    handleOfferId(id) {
        const offer = this.state.offers.filter(e => e.id === id)[0];
        if (offer) {
            this.setState({
                detailOffer: offer,
            }, () => {
                this.props.setIsShowDetailOffer();
            });
        }
        console.log('ID:', id)
    }
    resetDetailOffer(){
        console.log('resetdetailOffer: ', this);
        this.setState({
            detailOffer: undefined
        });
    }
    getFetchOffers() {
        fetch("/api/offers")
            .then(res => res.json())
            .then(result => {
                console.log(result);
                this.setState({
                    isLoading: false,
                    offers: result.data,
                })
            })
            .catch(err => {console.log(err)});
    }
    componentDidMount() {
        this.getFetchOffers();
    }

    render() {
        const list = this.state.offers.map(offers => {
            const {
                id,
                title,
                type,
                fonction,
                description,
                ville,
                date,
            } = offers;
            return (
                <div className="cadreLastOffer cadre " key={id}>
                    <div className="card-headers">
                        <p>{date.date}</p>
                        <FavoriteStar offerId={id}/>
                    </div>
                    <div className="card-content">
                        <p>{title}</p>
                        <p>{description}</p>
                    </div>
                    <div className="card-footer">
                        <p>{ville}/{type}</p>
                        <p><a href='#' onClick={()=>this.handleOfferId(id)}>See more</a></p>
                    </div>
                </div>
            );
        });
        console.log(list)

        return (
            <div>
                {this.state.detailOffer === undefined &&
                <div className='bg-gris'>
                    <h1>Last Offers</h1>
                    <div className="container">
                        
                        {list}
                    </div>
                    </div>}
                {this.state.detailOffer !== undefined && <DetailOffer resetDetailOffer={this.resetDetailOffer} handleApplyOffer={this.props.handleApplyOffer} detailOffer={this.state.detailOffer} setIsShowDetailOffer={this.setIsShowDetailOffer}/>}

            </div>);
    }
}
export default LastOffers;