function ContactUs(props){
    return (
        <div className="displayFlex justifieAround">
            <div className="displayBlock">
                <h2>Ready to get started?</h2>
                <h3>Sign up or Contact us</h3>
            </div>
            <div>
                <a onClick={()=>props.callback("SignUp")}><input type="button" className="input-Acceuil" value="SignUp"/></a>
                <a onClick={()=>props.callback("Contact")}><input type="button" className="input-Acceuil" value="Contact Us" /></a>
            </div>
        </div>
    )
}

export default ContactUs;