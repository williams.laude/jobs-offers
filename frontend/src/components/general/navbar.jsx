import React from "react";
import MenuHamburger from "./menuHamburger";

export default function  Navbar(props){
    console.log(props)
  return(
    <nav>
        <h1>We are Hiring</h1>
        <ul className="navUl">
          <li><a href="#home" onClick={()=>props.callback("Acceuil")}>Home</a></li>
          <li><a href="#offers" onClick={()=>props.callback("Offres")}>Offers</a></li>
          <li><a href="#my-application" onClick={()=>props.callback("MyAppli")}>My Applications</a></li>
          <li><a href="#favorite" onClick={()=>props.callback("Favorites")}>Favorites</a></li>
          <li><a href="#sign-in" onClick={()=>props.callback("SignIn")}>Login</a></li>
          <li><a href="#contact" onClick={()=>props.callback("Contact")}>Contact</a></li>
          <li><a href="#sign-up" onClick={()=>props.callback("SignUp")}><input type="button" value="Sign Up"/></a></li>
        </ul>
        <div>
          <div className="hamburger">
            <MenuHamburger/>
          </div>
        </div>
    </nav>
        )
}