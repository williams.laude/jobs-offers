import React, { Component } from 'react';
import starInactive from '../../Images/starInactive.png'
import starActive from '../../Images/starActive.png'


class FavoriteStar extends Component {
    constructor(props){
        super(props);
        this.state = {
            isFavoriteState: starInactive,
    
        }
    }
   

    setFavoriteStar() {

        if (this.state.isFavoriteState === starInactive) {
            const url = "http://localhost:3000/fav"
            this.setState({
                isFavoriteState: starActive
            });
            fetch(url, {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({
                    "offersId": this.props.offerId,
                    "userId": 1

                })


            })
            console.log("this props offerId", this.props.offerId)

        }

        else if (this.state.isFavoriteState === starActive) {
            const url = `http://localhost:3000/fav/${this.props.offerId}`
            this.setState({
                isFavoriteState: starInactive
            });
            fetch(url, {
                headers: {
                    'Content-Type': 'application/json'
                },
                method: "DELETE",
                body: null

            })
        }
    }

    render() {
        return (
            <div>
                <img onClick={() => this.setFavoriteStar()} width="20px" height="20px" src={this.state.isFavoriteState} alt="favoriteStar" />
            </div>
        );
    }
}

export default FavoriteStar;