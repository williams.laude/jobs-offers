import React, { Component } from 'react';

class Footer extends Component {
    state = {}
    render() {
        return (
        <footer className='footer'>
            <h3 className='footerLine'>We are Hiring</h3>
            <p className='footerText' >© 2022 Copyright Jobs Offers</p>
        </footer>);
    }
}

export default Footer;