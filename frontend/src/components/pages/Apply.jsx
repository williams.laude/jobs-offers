import React from "react";
import Header from "../general/header";

export default class Application extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            error: null,
            offers: [],
        }
    }

    getFetchOffersApply() {
        //let tab = []
        fetch('http://localhost:3000/candidatures')
            .then((response) => response.json())
            .then((candidatures) => {
                fetch('http://localhost:3000/offers')
                    .then((response) => response.json())
                    .then((offer) => {
                        offer.filter(offre => {
                            candidatures.filter(candidature => {
                                if (offre.id == candidature.offersId) {
                                    //tab.push(offre)
                                    this.setState({
                                        isLoading: false,
                                        offers: [...this.state.offers, offre]
                                    })
                                    console.log(offre)
                                }
                            })
                        })
                    })
            })
            .catch((err) => console.log(err))
    }

    componentDidMount() {
        this.getFetchOffersApply()
    }

    render() {
        const list = this.state.offers.map(offers => {
            const {
                title,
                type,
                ville,
                date
            } = offers;
            return (
                    <div className="cadreOffer">
                    <h1>{title}</h1>
                    <p>{type}</p>
                    <p>{ville}</p>
                    <p>{date}</p>
                    </div>

            )

        })
        console.log(list)
        return (
            <div>
                <Header name="My application" />
                
                {this.state.offers !== undefined && <div className="inlineCards">
                    {list}
                </div>}
            </div>
        )

    }

}