export default function ContactUsSend() {
    return (
        <>
            <h1>Contact Us</h1>
            <h2>Thanks !</h2>
            <h3>Your message as been send !</h3>
            <p>We thank you for your attention.<br/>
                We will respond as soon as possible.<br/>
                See you soon !</p>
        </>
    )
}