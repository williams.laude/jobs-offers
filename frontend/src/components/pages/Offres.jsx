import React from "react";
import FavoriteStar from "../general/favoriteStar";
import Header from "../general/header";
import DetailOffer from "./DetailOffer";
import Favorites from "./Favorites";

export default class Offers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            error: null,
            offers: [],
            detailOffer: undefined
        }
        this.resetDetailOffer = this.resetDetailOffer.bind(this);
    }


    handleOfferId(id) {
        const offer = this.state.offers.filter(e => e.id === id)[0];
        if (offer) {
            this.setState({
                detailOffer: offer
            });
        }
        console.log('ID:', id)
    }

    resetDetailOffer() {
        console.log('resetdetailOffer: ', this);
        this.setState({
            detailOffer: undefined
        });
    }
    getFecthOffers() {
        fetch('/api/offers')
            .then(response => response.json())
            .then(responseJson => {
                console.log(responseJson)

                this.setState({
                    isLoading: false,
                    offers: responseJson.data
                })
            })
            .catch(err => { console.log(err) })
    }
    componentDidMount() {
        this.getFecthOffers()
    }
    render() {
        const list = this.state.offers.map(offers => {
            const {
                id,
                title,
                type,
                fonction,
                description,
                ville,
                date,
            } = offers;
            console.log(offers);
            return (

                    <div className="cadreOffer" key={id}>
                        <div className="card-headers">
                            {date.date}
                            <FavoriteStar offerId={id}/>
                        </div>
                        <div className="card-content">
                            <h3>{title}</h3>
                            <p>{description}</p>
                        </div>
                        <div className="card-footer">
                            <p>Ville: {ville}/{type}</p>
                            <p><a href="#" onClick={() => this.handleOfferId(id)}>See More </a></p>
                        </div>
                </div>
            );
        });

        return (
            <div>
                <Header name="Offers" />
                {this.state.detailOffer === undefined && <div className="container">
                    {list}
                </div>}

                {this.state.detailOffer !== undefined && <DetailOffer resetDetailOffer={this.resetDetailOffer} handleApplyOffer={this.props.handleApplyOffer} detailOffer={this.state.detailOffer} />}
            </div>
        )
    }
}