import React, { Component } from 'react';
import Header from '../general/header';

export default class ApplyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      adress: '',
      codPostal: '',
      city: '',
      linkedin: '',
      gitLabHub: '',
      showForm: true,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    this.setState({ showForm: false })
    const url = "http://localhost:3000/candidatures"
    fetch(url, {
      headers: {
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({
        'firstName': this.state.firstName,
        'lastName': this.state.lastName,
        'email': this.state.email,
        'phone': this.state.phone,
        'adress': this.state.adress,
        'codPostal': this.state.codPostal,
        'city': this.state.city,
        'linkedin': this.state.linkedin,
        'gitLabHub': this.state.gitLabHub,
        'offerid': this.props.offerId,

      })

    })
    // console.log("Offer ID in ApplyForm handle submit", offerId)
      .then(responses => {
        console.log('Response fetch Handle submit', responses)
      })

    alert('Le nom a été soumis : ' + this.state.firstName + this.state.lastName + this.state.city + this.state.adress + this.state.codPostal + this.state.phone + this.state.linkedin + this.state.gitLabHub);
    event.preventDefault();
  }

  render() {
    return (
      <>
        <Header name="Apply" />
        {this.state.showForm && <form className='formSign marginAuto' onSubmit={this.handleSubmit}>
          <div className="textLeft">
            <label>Your Firstname</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='firstName' value={this.state.firstName} onChange={this.handleChange} required/><br></br>

            <label>Your Lastname</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='lastName' value={this.state.lastName} onChange={this.handleChange} required/><br></br>

            <label>
              Your email adress</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='email' value={this.state.email} onChange={this.handleChange} required/><br></br>

            <label>Your phone number</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='phone' value={this.state.phone} onChange={this.handleChange}/><br></br>

            <label>Your adress</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='adress' value={this.state.adress} onChange={this.handleChange}/><br></br>

            <label>Cod Postale</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='codPostal' value={this.state.codPostal} onChange={this.handleChange}/><br></br>
            <label>Ville</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='city' value={this.state.city} onChange={this.handleChange}/><br></br>

            <label>Your linkedin profil</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='linkedin' value={this.state.linkedin} onChange={this.handleChange}/><br></br>

            <label>Your GitLab/GitHub profil</label><br></br>
            <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="text" name='gitLabHub' value={this.state.gitLabHub} onChange={this.handleChange}/><br></br>
            <div className="buttonForm">
              <input type="submit" value="Apply" />
            </div>
          </div>
        </form>}

        {!this.state.showForm && <div>
          <h1>Thanks!</h1>
          <h3>Your application to {this.state.title} offer has been sent!</h3>
          <h4>We thank you for your application.</h4>
          <br></br>
          <h4>We will respond as soon as possible.</h4>
          <h4>See you soon </h4>
        </div>}
      </>
    );
  }
}