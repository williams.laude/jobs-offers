import React, {Component} from "react";
import Header from "../general/header";


class SignUp extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event)
    {
        this.setState({[event.target.name]: event.target.value})
    }

    handleSubmit(event)
    {
        if(this.state.confirmPassword === this.state.password){
            alert("enregister")
            this.props.callback('Acceuil')
            const url = "/api/register"
            fetch(url, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({
                'email': this.state.email,
                'password': this.state.password
            })
            })
            .then(responses => {
                console.log('Response fetch Handle submit', responses)
            })
            this.props.callback('Acceuil')
        }else{
            alert("La confirmation du mot de passe est incorrecte")
            this.props.callback('SignUp')
        }
    }

    render()
    {
        return(
            <div>
                <Header name="Sign Up" />
                <form className="formSign marginAuto" onSubmit={this.handleSubmit}>
                    <div className="textLeft">
                        <label htmlFor="email">Email</label>
                        <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="email" name="email" value={this.state.email} onChange={this.handleChange} required/>
                        
                        <label htmlFor="password">Password</label>
                        <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="password" name="password" value={this.state.password} onChange={this.handleChange} minLength="3" maxLength="16" required/>
                        
                        <label htmlFor="confirmPassword">Confirm password</label>
                        <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="password" name="confirmPassword" value={this.state.confirmPassword} onChange={this.handleChange} required/>
                    </div>

                    <input className="blackButton" type="submit" value="Sign Up"/>
                </form>
                <p>You already have an acount yet?</p>
                <a className="black" href="#" onClick={() => this.props.callback('SignIn')}>Sign in</a>
            </div>
        )
    }
}

export default SignUp;