import React, { Component } from 'react';
import ApplyForm from './ApplyForm';
import Offers from './Offres';
import Header from '../general/header';
import FavoriteStar from '../general/favoriteStar';

class DetailOffer extends React.Component {
    state = {
        showDetailOffer: true,
        error: null,
        offers: [],
        offerId: 0
    }

    handleOffer () {
       const {id} = this.props.detailOffer;
       console.log('detailOfferId',id);
       this.props.handleApplyOffer(id);

    }
    fetchDetailOffer() {
        fetch('/api/offers/{offers.id}')
            .then(response => response.json())
            .then(responses => {
                console.log(responses)
                this.setState({
                    isLoading: false,
                    offers: responses,
                    offerId: this.state.offers.id
                })
            })
            .catch(err => { console.log(err) })

    }
    componentDidMount() {
        // this.fecthDetailOffer()
    }

    render() {
        const {
            id,
            title,
            type,
            fonction,
            description,
            ville,
            date,
        } = this.props.detailOffer;
        // console.log(this.state.offers.id)
        return (
            <div key={id}>
                <p>Title: {title}</p>
                <FavoriteStar offerId={this.state.offerId}/>
                <p>Type: {type}</p>
                <p>Fonction: {fonction}</p>
                <p>Description: {description}</p>
                <p>Ville: {ville}</p>
                <p>Date: {date.date}</p>
                <button onClick={() => this.handleOffer()}>Apply</button>
                <br />
                <a href="#" onClick={this.props.resetDetailOffer}>Back to offers</a>
            </div>

        );
    }
}

export default DetailOffer;