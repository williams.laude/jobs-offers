import React, { Component } from "react";
import Header from "../general/header";
import DetailOffer from "../pages/DetailOffer";

class Favorites extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            error: null,
            detailOffer: undefined,
            favorites: [],
        }
        this.resetDetailOffer = this.resetDetailOffer.bind(this);
    }

    handleOfferId(id) {
        const offer = this.state.favorites.filter(e => e.id === id)[0];
        if (offer) {
            this.setState({
                detailOffer: offer
            });
        }
        console.log('ID:', id)
    }

    resetDetailOffer() {
        console.log('resetdetailOffer: ', this);
        this.setState({
            detailOffer: undefined
        });
    }


    getFavorites() {
        fetch('http://localhost:3000/fav')
            .then((data) => data.json())
            .then((datas) => {
                fetch('http://localhost:3000/offers')
                    .then(response => response.json())
                    .then(offers => {
                        offers.filter(offre => {
                            datas.filter(data => {
                                console.log("offersid: " + data.offersId)
                                console.log("Id offre: " + offre.id)
                                if (data.offersId == offre.id) {
                                    this.setState({
                                        isLoading: false,
                                        favorites: [...this.state.favorites, offre]
                                    })
                                    console.log(this.state.favorites)
                                }
                            })
                        })
                    })
            })
    }

    componentDidMount() {
        this.getFavorites();
    }
    render() {
        console.log(this.props)
        const list = this.state.favorites.map(favorites => {
            const {
                id,
                date,
                title,
                description,
                ville,
                type,

            } = favorites;
            return (
                <div className="cadreOffer">
                    <p>{date}</p>
                    <h2>{title}</h2>
                    <p>{description}</p>
                    <p>{ville}</p>
                    <p>{type}</p>
                    <a href="#" onClick={() => {
                        this.handleOfferId(id)
                        //this.props.callback("Detail")

                    }
                    }>See More</a>
                </div>

            )

        })
        return (
            <div>
                <Header name="Favorites" />
                {this.state.detailOffer === undefined && <div className="inlineCards">
                    {list}
                </div>}

                {this.state.detailOffer !== undefined && <DetailOffer resetDetailOffer={this.resetDetailOffer} handleApplyOffer={this.props.handleApplyOffer} detailOffer={this.state.detailOffer} />}
            </div>
        )
    }
}

export default Favorites;