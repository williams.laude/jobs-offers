import React, {Component} from "react";
import Header from "../general/header";


class SignIn extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            email: '',
            password: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event)
    {
        this.setState({[event.target.name]: event.target.value})
    }

    handleSubmit(event)
    {
        alert('connecter')
        this.props.callback('Apply')
    }

    render()
    {
        return(
            <>
                <Header name="Sign In" />
                <form className="formSign marginAuto" onSubmit={this.handleSubmit}>
                    <div className="textLeft">
                        <label htmlFor="email">Email</label>
                        <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="email" name="email" value={this.state.email} onChange={this.handleChange} required/>

                        <label htmlFor="password">Password</label>
                        <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="password" name="password" value={this.state.password} onChange={this.handleChange} required/>
                        <div className="buttonForm">
                            <input type="submit" value="Sign In"/>
                        </div>
                    </div>
                </form>
                <p>Don't have an acount yet?</p>
                <a className="black" href="#" onClick={() => this.props.callback('SignUp')}>Create an acount</a>
            </>
        )
    }
}

export default SignIn;