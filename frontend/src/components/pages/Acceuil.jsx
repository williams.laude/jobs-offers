import React from "react";
import Header from "../general/header";
import ContactUs from "../Accueil/contactus";
import About from "../Accueil/about";
import LastOffers from "../Accueil/LastOffers";
import Viewoffers from "../Accueil/headeroffers";
import Offers from "./Offres";


export default class Acceuil extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            page: '',
            isShowDetailOffer: false,

        };
        this.handlePageChange = this.handlePageChange.bind(this);
        this.setIsShowDetailOffer = this.setIsShowDetailOffer.bind(this)
        console.log("props detailOffer", this.props.detailOffer)
    }

    setIsShowDetailOffer() {
        this.setState({
            isShowDetailOffer: true
        })
    }

    handlePageChange(string) {
        this.props.callback(string);
        
    }

    render() {
        return (

            <div>
                {this.state.isShowDetailOffer === false && <div className="bg-gris">
                    <Header name="Finding a job has never been easier" />
                    <Viewoffers callback={this.handlePageChange} />
                    <About />
                </div>
                }
                <LastOffers setIsShowDetailOffer={this.setIsShowDetailOffer} handleApplyOffer={this.props.handleApplyOffer} />

                {this.state.isShowDetailOffer === false && <ContactUs callback={this.handlePageChange} />}


            </div>
        )
    }
}