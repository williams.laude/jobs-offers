import React, {Component} from "react";
import Header from "../general/header";

class Contact_Us extends React.Component
{
    constructor(props){
        super(props);
        this.state={
            email: '',
            message: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event){
        this.setState({[event.target.name]: event.target.value});
        
    }
    
    handleSubmit(event){
        alert('Le message a été envoyé: ' + this.state.email + '|' +this.state.message);
        fetch('http://localhost:3000/message',{
            headers: {'Content-Type': 'application/json'},
            method: 'POST',
            body: JSON.stringify({
                'email': this.state.email,
                'message': this.state.message
            }),
        }).then(response =>{
            console.log('Requete OK')
        })
        this.props.callback('ContactUsSend')
        event.preventDefault();
    }

    render()
    {
        return(
            <>
                <Header name="Contact Us" />
                <form className="formSign marginAuto" onSubmit={this.handleSubmit}>
                <div className="textLeft">
                    <label htmlFor="email">Your email adress</label>
                    <input className="inputFormBorder bg-white inputFormDimention inputSign displayBlock" type="email" name="email" value={this.state.email} onChange={this.handleChange} required/>
                    
                    <label htmlFor="message">Your message</label>
                    <textarea className="inputFormBorder bg-white textareaFormDimention inputSign displayBlock" type="text" name="message" value={this.state.message} onChange={this.handleChange} required/>
                    
                    <div className="buttonForm">
                        <input type="submit" value="Send"/>
                    </div>
                </div>
                </form>
            </>
            
        )
    }
}

export default Contact_Us